﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {



	// Use this for initialization
	void Start () {
		StartCoroutine(loadScene());
	}

	IEnumerator loadScene(){
		AsyncOperation async = SceneManager.LoadSceneAsync("room", LoadSceneMode.Single);
		async.allowSceneActivation = false;
		while(async.progress <= 0.89f){
		//	progressText.text = async.progress.ToString();
			yield return null;
		}
		Dictionary<string,string> ob = new Dictionary<string, string>();
		ob.Add ("action", "init");

		Server.instance.POST(ob);
		async.allowSceneActivation = true;
	}
}
