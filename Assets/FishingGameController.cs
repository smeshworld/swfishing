﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishingGameController : MonoBehaviour {

	// Use this for initialization
	public GameObject RodLine;
	public GameObject RodEnd;
	public GameObject Rod;

	public GameObject[] prefabGifts;
	public GameObject[] prefabObstacles;

	private float side  = 1f;
	private bool canRotate = false;
	private bool isDown = false;
	private GameObject claimed;
	private Vector3 claimedPoint; 

	private List<GameObject> objects = new List<GameObject>();

	void Start () {
		canRotate = true;
		generateLevel ();
		Debug.Log ((int)300f);
	}

	void generateLevel()
	{
		float rndx;
		float rndy;

		for (int i = 0; i < 5; i++) {
			rndx = Random.Range (-5f, 5f);
			rndy = Random.Range(-1.10f,  1.10f);

			var randomPosition = new Vector3(rndx, rndy, -2);

			GameObject g = Instantiate(prefabGifts [Random.Range (0, prefabGifts.Length - 1)], randomPosition,  Quaternion.identity) as GameObject;;
			objects.Add (g);
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log("collision");
	}
	
	// Update is called once per frame
	void Update () {
		if (/*(!isDown && Rod.transform.localEulerAngles.z> 44.5f  && Rod.transform.localEulerAngles.z < 45.5f)  ||*/  (!isDown && Input.GetKeyDown(KeyCode.Space))) {
			canRotate = false;
			isDown = true;

			float a = Mathf.Abs(Rod.transform.localEulerAngles.z);
			float catheter = 3.01f;
			Debug.Log ("angle is " + a);
			if (a > 180f)
				a = 360f - a;
			Debug.Log ("cos(" + a + ") ="  + Mathf.Cos (a  * Mathf.PI/180));

			catheter = catheter + RodEnd.transform.position.y - 1.6f;
			float h = Mathf.Abs(1f/Mathf.Cos((a) * Mathf.PI/180) * (catheter));

			Debug.Log ("h by cos" + h);

			/*if(a  > 45 && a < 315)
			{
				Debug.Log ("by sin");
				h =  Mathf.Abs(1/Mathf.Sin((a) * Mathf.PI/180) * (4.66f));
				catheter = 4.96f;
			
			}*/
			Debug.Log ("h by sin" + Mathf.Abs(1/Mathf.Sin((a) * Mathf.PI/180) * (4.66f)));
		//	h = -h;
			//h = h + 0.7f;
			Vector3 pos = RodEnd.transform.TransformPoint (Vector3.zero);
			Debug.Log ("global n y: " + pos.y);
			Vector3 localPos = Vector3.zero - Rod.transform.position - RodEnd.transform.position;
			Debug.Log ("local y" + localPos.y);
			Debug.Log ("local x" + localPos.x);
			Debug.Log ("Rod y" + Rod.transform.position.y);
			Debug.Log ("RodEnd y" + RodEnd.transform.position.y);
			Debug.Log ("h: " + h);

			h = 1.6f - 3.01f;

			//float x = Mathf.Sqrt (h*h- catheter*catheter);

			//float x2 = Mathf.Sqrt ((h-0.7f)*(h-0.7f)- catheter*catheter);
			//Debug.Log ("X2 " + x2); // гуд
			float x = Mathf.Abs(Mathf.Tan(a * Mathf.PI/180) * (h - pos.y));
			if (a > 45f) {

				x = 4.66f;
				Debug.Log ("tan (" + a + ")  = " + Mathf.Tan (a * Mathf.PI / 180));
				h = Mathf.Abs (x / Mathf.Tan (a * Mathf.PI / 180));
				Debug.Log ("By height " + h);
				h = Rod.transform.position.y - h;
			} 
		
			if (Rod.transform.localEulerAngles.z > 180)
				x = -x;
			if(a <= 45f)
				x = x -  localPos.x;
			Debug.Log ("X: " + x + " h: " + h + " cath: " + (h - pos.y));
			//-3.652138-2.7+1.7
			StartCoroutine (MoveOverSpeed (RodEnd, new Vector3 (x, h), 5f, RodEnd.transform.position.y));

		}
			
		if (canRotate) {
			moveRod ();
		} else if(isDown)
		{
			moveDown ();
		}
	}


	void moveDown() {
		var pos = RodEnd.transform.position;

		//RodEnd.transform.position = new Vector3 (pos.x, pos.y-1*Time.deltaTime, pos.z);
		//RodLine.transform.localScale = new Vector3(RodLine.transform.localScale.x, RodLine.transform.localScale.y+1*Time.deltaTime, RodLine.transform.localScale.z);
	}

	void moveRod() {
		float angle = Rod.transform.localEulerAngles.z + 50f * Time.deltaTime * side;
	
		int a = Mathf.RoundToInt(Rod.transform.localEulerAngles.z);
		if (a > 180) {
			a = 360 - a;
		}
		/*if (Rod.transform.localEulerAngles.z <= 300 && Rod.transform.localEulerAngles.z >= 180) {
			Debug.Log ("change from " + side + " z:" + Rod.transform.localEulerAngles.z);
			side = -side;
			angle = 
		} else */if (a > 60) {
			Debug.Log ("change right from " + side + " z:" + Rod.transform.localEulerAngles.z + " a: " + a);
			angle = Rod.transform.localEulerAngles.z > 180?300f:60f;
			Debug.Log ("new angle" + angle);
			side = -side;
		}

		Rod.transform.localEulerAngles = new Vector3 (Rod.transform.localEulerAngles.x, Rod.transform.localEulerAngles.y,  angle);

	}


	public IEnumerator MoveOverSpeed (GameObject objectToMove, Vector3 end, float speed, float startpos){
		// speed should be 1 unit per second
		//Debug.Log("start move");
		//RodLine.transform.position.y - startpos;
		Vector3 start = objectToMove.transform.position;
		while (objectToMove.transform.position != end)
		{
			if (claimed) {
				break;
			}
			//Debug.Log ("objectToMove x: " + objectToMove.transform.position.x + "y: " + objectToMove.transform.position.y + " Line y " + (startpos - RodEnd.transform.position.y));
			objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
			RodLine.transform.localScale = new Vector3(RodLine.transform.localScale.x, (RodLine.transform.position.y-RodEnd.transform.position.y)/Mathf.Abs(RodLine.transform.position.y - startpos), RodLine.transform.localScale.z);
			yield return new WaitForEndOfFrame ();
		}
		end = start;
		if (claimed) {
			speed = 2f;
			end = Rod.transform.position;
		}


		while (objectToMove.transform.position != end)
		{
			
			//Debug.Log ("objectToMove x: " + objectToMove.transform.position.x + "y: " + objectToMove.transform.position.y + " Line y " + (startpos - RodEnd.transform.position.y));
			objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
			RodLine.transform.localScale = new Vector3(RodLine.transform.localScale.x, (RodLine.transform.position.y-RodEnd.transform.position.y)/Mathf.Abs(RodLine.transform.position.y - startpos), RodLine.transform.localScale.z);
			if (claimed) {
				claimed.transform.position = Vector3.MoveTowards (objectToMove.transform.position - claimedPoint, end, speed * Time.deltaTime);
			}
			yield return new WaitForEndOfFrame ();
		}


		if (claimed) {
			claimed.transform.position = new Vector3 (Random.Range(0.17f,3.18f), Random.Range(2.77f, 3.08f), 1f);
			claimed.transform.localScale = new Vector3 (0.9f, 0.9f, claimed.transform.localScale.z);
			end = start;
			while (objectToMove.transform.position != end)
			{

				//Debug.Log ("objectToMove x: " + objectToMove.transform.position.x + "y: " + objectToMove.transform.position.y + " Line y " + (startpos - RodEnd.transform.position.y));
				objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
				RodLine.transform.localScale = new Vector3(RodLine.transform.localScale.x, (RodLine.transform.position.y-RodEnd.transform.position.y)/Mathf.Abs(RodLine.transform.position.y - startpos), RodLine.transform.localScale.z);
				yield return new WaitForEndOfFrame ();
			}
		}
			

		canRotate = true;
		isDown = false;
		claimed = null;


	}

	public void onGiftClaim(GameObject go)
	{
		claimedPoint = RodEnd.transform.position - go.transform.position;
		Debug.Log ("claimed Point " + claimedPoint);
		claimed = go;
	}
}
