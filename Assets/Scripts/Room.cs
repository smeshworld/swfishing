﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Room : MonoBehaviour {
	public GameObject Interface;
	public SpriteRenderer spriteRenderer;
	private bool isLoaded = false;
	public Text LoadingText;
	private float width;
	private int screenWidth;
	private float max;
//	private Sprite room;

	// Use this for initialization
	/*void Start () {
		StartCoroutine(LoadRoom("http://smeshworld.ru/unity/rooms/lake_meteor.png"));
	}*/

	IEnumerator Start () {
		WWW www = new WWW ("http://smeshworld.ru/unity/rooms/Aquapark_Inside_Location.png");
		while (!www.isDone) {
			LoadingText.text = (www.progress * 100).ToString () + "%";
			//Debug.Log (www.progress);
			yield return null;
		}

		Dictionary<string,string> ob = new Dictionary<string, string>();
		ob.Add ("action", "getRoom");
		ob.Add ("id", "1");
		Server.instance.POST (ob);
		LoadingText.text = "Генерация локации";
		Debug.Log ("loaded " +  www.texture.width + " " + www.texture.height);
		//r.GetComponent<RawImage>().texture = www.texture;
		//room = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
		Sprite sprite =  Sprite.Create(www.texture, new Rect(0.0f, 0.0f, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f), 100.0f);
//		r.GetComponent<Sprite> =  Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
		width = (float)www.texture.width/200.0f;
		spriteRenderer.sprite = sprite;
		isLoaded = true;
		showInterface ();
		screenWidth = Screen.width;
		Debug.Log (Camera.main.GetComponent<Camera>().transform.right);

		Debug.Log ("width" + screenWidth + "resol" + Screen.currentResolution);
		max = width - 4.96f;

	}

	void onRoomGetData()
	{
		Debug.Log ("callback ok");
	}

	void showInterface()
	{
		Interface.SetActive (true);


	}

	void Update()
	{
		if (!isLoaded)
			return;
		
		if (Input.GetAxis ("Horizontal") != 0) {
			Debug.Log ("line" + max);
			Vector3 pos = spriteRenderer.transform.position;
			pos.x = pos.x + 4.0f * Time.deltaTime * -Input.GetAxis ("Horizontal");
			if (pos.x < -max) {
				pos.x = -max;
			} else if (pos.x > max) {
				pos.x = max;
			}
			spriteRenderer.transform.position = new Vector3 (pos.x, 0.0f);
			Debug.Log (pos.x);
		}
	}
		


}
