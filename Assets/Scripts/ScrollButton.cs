﻿using UnityEngine;
using UnityEngine.EventSystems;


public class ScrollButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

	public bool isOver = false;

	// Use this for initialization
	void Start () {
		Debug.Log ("On Scroll init");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		isOver = true;
		Debug.Log ("onPointerEnter");
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		Debug.Log("Mouse exit");
		isOver = false;
	}
}
