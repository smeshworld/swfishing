﻿using UnityEngine;
using System.Collections;

public class RodEnd : MonoBehaviour {

	public FishingGameController gc;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log("collision rod end");
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		Debug.Log("trigger rod end" + coll.name);
		if(coll.gameObject.GetComponent<BoxCollider2D>())
			coll.gameObject.GetComponent<BoxCollider2D>().enabled = false;
		else
			coll.gameObject.GetComponent<CircleCollider2D>().enabled = false;
		
		gc.onGiftClaim (coll.gameObject);

		
	}
}
